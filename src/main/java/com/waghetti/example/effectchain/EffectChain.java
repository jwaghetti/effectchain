package com.waghetti.example.effectchain;

import com.waghetti.example.effectchain.effect.Effect;

import java.util.LinkedList;
import java.util.List;

public class EffectChain {

    private List<Effect> effects = new LinkedList<>();

    public void addEffect(Effect effect) {
        effects.add(effect);
    }

    public String getChain() {

        StringBuilder chain = new StringBuilder("Input -> ");

        for (Effect effect : effects) {
            chain.append(effect.getName());
            chain.append(" -> ");
        }

        chain.append("Output");

        return chain.toString();
    }
}
