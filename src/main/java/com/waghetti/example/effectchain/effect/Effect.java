package com.waghetti.example.effectchain.effect;

import org.atteo.classindex.IndexSubclasses;

@IndexSubclasses
public interface Effect {

    String getName();

}
