package com.waghetti.example.effectchain.effect;

public class Flanger implements Effect {

    @Override
    public String getName() {
        return "Flanger";
    }
}
