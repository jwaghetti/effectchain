package com.waghetti.example.effectchain.effect;

import org.atteo.classindex.ClassIndex;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Locale;

public class EffectFactory {

    private final HashMap<String, Class<Effect>> effects = new HashMap<>();
    private static final EffectFactory instance = new EffectFactory();

    static {
        for (Class clazz : ClassIndex.getSubclasses(Effect.class)) {
            String[] className = clazz.getName().split("\\.");
            instance.addEffect(className[className.length-1].toLowerCase(Locale.ENGLISH), clazz);
        }
    }

    public static EffectFactory getInstance() {
       return instance;
    }

    public void addEffect(String key, Class<Effect> effect) {
        if (isEffectClassSuitable(effect)) {
            effects.put(key, effect);
        }
    }

    public Effect getEffect(String key) throws NoSuchEffectException {

        Effect effect = instantiateEffectClass(effects.get(key));

        if (effect == null) {
            throw new NoSuchEffectException();
        }

        return effect;

    }

    private Effect instantiateEffectClass(Class<Effect> clazz) {
        try {
            if (clazz != null) {
                return clazz.getConstructor().newInstance();
            }
        } catch (NoSuchMethodException
                | InstantiationException
                | IllegalAccessException
                | InvocationTargetException exception) {
        }

        return null;
    }

    private boolean isEffectClassSuitable(Class<Effect> clazz) {

        boolean suitable = true;

        try {
            clazz.getConstructor();
        } catch (NoSuchMethodException exception) {
            suitable = false;
        }

        return suitable;

    }

}
