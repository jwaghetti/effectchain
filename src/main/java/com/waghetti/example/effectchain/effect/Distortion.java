package com.waghetti.example.effectchain.effect;

public class Distortion implements Effect {

    @Override
    public String getName() {
        return "Distortion";
    }

}
