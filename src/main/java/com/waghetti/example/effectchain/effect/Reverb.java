package com.waghetti.example.effectchain.effect;

public class Reverb implements Effect {

    @Override
    public String getName() {
        return "Reverb";
    }
}
