package com.waghetti.example.effectchain.effect;

public class Overdrive implements Effect {

    @Override
    public String getName() {
        return "Overdrive";
    }
}
