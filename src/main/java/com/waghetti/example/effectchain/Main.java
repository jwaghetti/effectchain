package com.waghetti.example.effectchain;

import com.waghetti.example.effectchain.effect.EffectFactory;
import com.waghetti.example.effectchain.effect.NoSuchEffectException;

import java.util.Scanner;

public class Main {

    private static EffectChain chain = new EffectChain();

    public static void main(String[] args) {

        while(true) {
            System.out.println("Enter effect name (lowercase) or 'end' to terminate the chain:");
            processCommand(readCommand());
        }

    }

    private static void addEffect(String effect) {
        try {
            chain.addEffect(EffectFactory.getInstance().getEffect(effect));
        } catch (NoSuchEffectException exception) {
            System.out.println("Effect unavailable");
        }
    }

    private static void printChain() {
        System.out.println("-------- Effect Chain:");
        System.out.println(chain.getChain());
        System.exit(0);
    }

    private static void processCommand(String command) {
        if ("end".equals(command)) {
            printChain();
            System.exit(0);
        } else {
            addEffect(command);
        }
    }

    private static String readCommand() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

}
